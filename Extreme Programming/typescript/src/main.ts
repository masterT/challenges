export enum Delays {
  Short = 50,
  Medium = 200,
  Long = 400,
}

export async function fetchMessageByUserId(id: number): Promise<string> {

  return new Promise<string>((resolve, reject) => {
    fetchUserNameById(id, (_, userName: string) => {
      if (userName) {
        fetchMessageByUserName(userName, (_, message) => {
          if (message) {
            resolve(message);
          } else {
            reject('error');
          }
        })
      } else {
        reject('error');
      }
    });
  });
}

function fetchUserNameById(id: number, callback): void {
  const userName: string = 'Bob';

  setTimeout(
    () => {
      if (id === 123) {
        callback(0, userName);
      } else {
        callback(1);
      }
    },
    Delays.Long);
}

function fetchMessageByUserName(name: string, callback): void {
  const message: string = 'Hi!';

  setTimeout(
    () => {
      if (name === 'Bob') {
        callback(0, message);
      } else {
        callback(1);
      }
    },
    Delays.Medium);
}
