import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import Vuex from 'vuex'
import router from './router'

Vue.use(Vuex)
Vue.use(VueRouter)

const store = new Vuex.Store({
  state: {
    url: '',
    valid: false
  },
  mutations: {
    setValid (state, valid) {
      state.valid = valid
    },
    setUrl (state, url) {
      state.url = url
    }
  }
})

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
