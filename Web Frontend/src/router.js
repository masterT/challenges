import VueRouter from 'vue-router'
import Home from './views/Home'
import Qr from './views/Qr'
import Frame from './views/Frame'


const router = new VueRouter({
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      path: '/qr',
      component: Qr
    },
    {
      path: '/frame',
      component: Frame
    }
  ]
})


export default router
