# Reverse Engineering

## I like to MOV it, MOV it !

Bienvenue au challenge de reverse engineering. Vous devez analyser un petit programme écrit en C nommé `main`, puis compilé, dont le fonctionnement interne est méconnu. Plus concrètement, vous devez produire le diagramme de flot des fonctions (branchements) (control flot) au format Graphviz (.dot).

### Digraphes

Le format Graphviz (.dot) permet de créer des digraphes facilement et on peut les visualiser en ligne ici: http://www.webgraphviz.com/

e.g. 

```
digraph calls {
f2 -> f2;
f2 -> f3;
f1 -> f2;
f1 -> f5;
f3 -> f5;
f2 -> f5;
}
```

![](exampleDigraph.png)

### Indices

L'assembleur du programme a été obfusqué par une technique secrète:  à vous de découvrir son fonctionnement et comment la contourner! 

Cet article pourrait vous mettre sur la piste: http://www.cl.cam.ac.uk/~sd601/papers/mov.pdf


Bonne chance!

